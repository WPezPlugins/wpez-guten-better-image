<?php
/**
 * Plugin Name: WPezPlugins: Guten Better - Image Block
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-guten-better-image
 * Description: Makes the WordPress Gutenberg Image block more intelligent via the img tag's sizes and srcset properties.
 * Version: 0.0.9
 * Author: Mark Simchock CEA at Alchemy United
 * Author URI: https://AlchemyUnited.com
 * License: GPLv2 or later
 * Text Domain: wpez-gbi
 *
 * @package WPezGutenBetterImage
 */

namespace WPezGutenBetterImage;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

use WPezGutenBetterImage\App\{
	Config\ClassConfig as Config,
	ClassPlugin as Plugin,
};

$str_php_ver_comp = '7.4.0';

if ( version_compare( PHP_VERSION, $str_php_ver_comp, '<' ) ) {
	exit( sprintf( 'WPezPlugins: Guten Better Image Block requires PHP ' . esc_html( $str_php_ver_comp ) . ' or higher. Your WordPress site is using PHP %s.', PHP_VERSION ) );
}

/**
 * Setup our autoloader.
 *
 * @param boolean $bool A simple toggle to continue or not.
 *
 * @return bool
 */
function autoloader( bool $bool = true ) {

	if ( false === $bool ) {
		return;
	}

	require_once 'App/Lib/Autoload/ClassAutoload.php';

	$new_autoload = new ClassAutoload();

	$new_autoload->setPathBase( plugin_dir_path( __FILE__ ) );
	$new_autoload->setNeedleRoot( __NAMESPACE__ );
	$new_autoload->setNeedleChild( 'App' );

	return spl_autoload_register( array( $new_autoload, 'wpezAutoload' ), true );
}
autoloader();

/**
 * Inits the main plugin file.
 *
 * @param boolean $bool A simple toggle to continue or not.
 *
 * @return void
 */
function initPlugin( $bool = true ) {

	if ( false === $bool ) {
		return;
	}

	$new_config = new Config( __DIR__, plugins_url( null, __FILE__ ) );

	// Make the config available to other plugins.
	$new_config = apply_filters( __NAMESPACE__ . '/expose_config', $new_config );
	// If we don't get an instance of the Config back from the filter then whip up another instance of the config.
	if ( ! $new_config instanceof Config ) {
		$new_config = new Config( __DIR__, plugins_url( null, __FILE__ ) );
	}

	$new_plugin = new Plugin( $new_config );
}
add_action( 'init', __NAMESPACE__ . '\initPlugin' );
