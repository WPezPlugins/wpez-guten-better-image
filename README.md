## WPezGutenBetter Image

__Utilizes the WordPress Gutenberg Image block's settings, as well as the (theme's) content width, to improve page load speed by optimizing the image tags' sizes and srcset values.__


### OVERVIEW

Left to its own devices, WordPress can make sub-optimal use of the img tags' sizes and srcset values. The good news is, the Gutenberg Image block's attributes can be used to mitigate native WP's shortcomings.

If you're not familiar with the img tags' sizes and srcset there are helpful links listed below. That said, contrary to conventional wisdom it is the
sizes that matters most, not the srcset. 


---
>
> IMPORTANT
> 
> You can use this plugin alone with its defaults, but for best results it's recommended you customize your settings via this other plugin:
>
> See also: https://gitlab.com/WPezPlugins/wpez-guten-better-image-for-todo
> 
--- 


### FAQ

__1 - Why?__

Because page load speed matters, it affects SEO and UX. Sub-optimal image sizes' settings can also lead to unnecessary data plan over-consumption.

__2 - Can you give an example?__

Absolutely. Without this plugin, when an image block's alignment is set to 'center' and the image size selected has a width (e.g., 1500px) greater than the theme's content width (e.g., 950px), regular Gutenberg will render that image's sizes as:

```
sizes="(max-width: 1500px) 100vw, 1500px"
```

The problem is the max-width. It's wider than it needs to be, which means it's going to encourage the browser to make less than ideal decisions about which image to pick from the srcset. Remember that (for most themes) alignment 'center' keeps the image within the theme's main content container. Therefore, the max-width an image can be is the theme's content width, not the image's width.

On the other hand, this plugin will use the content width and render a much improved:

```
sizes="(max-width:950px) 100vw, 950px"
```

The difference between 1500px and 950px is significant. Now imagine if your client selected Full Size (for Image Size) and that image was 3500px wide, or more and/or there are multiple images on the page.

When alignment is wide there are similar opportunities that beg to be fixed - which are also addressed by this plugin.

__3 - But doesn't lazy loading fix this?__

Kind of, but not really. Lazy load will delay images from being rendered but it does not help the browser make better decisions (based on an image's sizes and srcset). The browser requesting an image that's larger than necessary is never a good thing.


__4 - How can I tell the plugin not to work its magic on a particular image?__

Under the block's Advanced settings, in the Additional CSS class(es) input, add the class: 

```
wpez-gbi-false
``` 

__5 - Are there filters to customize how the plugin works?__

Yes! Please see the first link in the Helpful Links section below. That plugin (which was also mentioned above) is boilerplate code with all the filters for this plugin. 

__6 - Is there anything else I need to know?__

Not really. Only that I encourage you to install the plugin and give it a spin. It doesn't add anything to the database, and if deactivated or uninstalled your image blocks will return to normal. No hard. No foul.

__7 - I'm not a developer, can we hire you?__

Yes, that's always a possibility. If I'm available, and there's a good mutual fit.


### Helpful Links

- https://gitlab.com/WPezPlugins/wpez-guten-better-image-for-todo

- https://ericportis.com/posts/2014/srcset-sizes/

- https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images

- https://make.wordpress.org/core/tag/4-4/#post-15628
 
- https://viastudio.com/optimizing-your-theme-for-wordpress-4-4s-responsive-images/
 
- https://medium.com/@duongphan/controlling-responsive-images-in-wordpress-breaking-down-the-code-7d36dd273787

- https://gitlab.com/WPezPlugins/wpez-image-location


### TODO

- Write a blog article that's more detailed than this README.


### CHANGE LOG

__-- 0.0.9 - Fri 5 Nov 2021__

- Bug fixes after extensive testing.


__-- 0.0.8 - Wed 7 July 2021__

- Refactored top to bottom.


__-- 0.0.7 - Mon 20 Jan 2020__

- UPDATED: ClassPlugin->filter() priority of hook *_core__image changed from 10 (first) to 50 (last)


__-- 0.0.6 - Tue 15 Oct 2019__

- UPDATED - Fixed reset(). Turns out wp_calculate_image_srcset fired before wp_calculate_image_sizes (not after).


__-- 0.0.5 - Tue 15 Oct 2019__

- INIT - Hey! Ho!! Let's go!!!

   