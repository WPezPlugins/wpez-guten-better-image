<?php
/**
 * The custom WPez autoloader.
 * 
 * @package WPezGutenBetterImage
 */
namespace WPezGutenBetterImage;

if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * TODO - add comments, doc blocks, etc.
 */
class ClassAutoload {

	protected $str_needle_root;
	protected $str_needle_child;
	protected $strpath_base;
	protected $str_remove_from_class;

	private $version;
	private $url;
	private $path;
	private $path_parent;
	private $basename;
	private $file;


	public function __construct( $arr_args = array() ) {

		$this->setup();

		$this->setPropertyDefaults();

		spl_autoload_register( null, false );

	}

	/**
	 *
	 */
	protected function setup() {

		$this->version     = '0.5.0';
		$this->url         = plugin_dir_url( __FILE__ );
		$this->path        = plugin_dir_path( __FILE__ );
		$this->path_parent = dirname( $this->path );
		$this->basename    = plugin_basename( __FILE__ );
		$this->file        = __FILE__;
	}

	protected function setPropertyDefaults() {

		$this->str_needle_root       = __NAMESPACE__;
		$this->str_needle_child      = false;
		$this->str_path_base         = false;
		$this->str_remove_from_class = __NAMESPACE__;
	}

	public function setNeedleRoot( string $str ) {

		$this->str_needle_root = $str;
		$this->str_remove_from_class = $str;
	}

	public function setNeedleChild( string $str ) {

		$this->str_needle_child = $str;
	}

	public function setPathBase( string $str ) {

		$this->str_path_base = $str;
	}

	public function setRemoveFromClass( string $str ) {

		$this->str_remove_from_class = $str;
	}


	/**
	 * (@link http://www.phpro.org/tutorials/SPL-Autoload.html)
	 *
	 * The classes naming convention allows us to parse the folder structure
	 * out of the class / file name. And then use that to define the $file for
	 * the require_once()
	 */
	public function wpezAutoload( $str_class ) {

		if ( false === strrpos( $str_class, $this->str_needle_root, -strlen( $str_class ) ) ) {
			return false;
		}

		$arr_class = explode( '\\', $str_class );

		if ( count( $arr_class ) < 3 || false === $this->str_needle_child || ! isset( $arr_class[1] ) || $arr_class[1] !== $this->str_needle_child ) {
			return false;
		}

		$str_file = implode( DIRECTORY_SEPARATOR, $arr_class );
		$str_file = ltrim( str_replace( $this->str_remove_from_class, '', $str_file ), DIRECTORY_SEPARATOR );
		$str_file = rtrim( $this->str_path_base, "/" ) . DIRECTORY_SEPARATOR . $str_file . '.php';

		if ( file_exists( $str_file ) ) {
			require $str_file;

			return true;
		}

		return false;
	}
}
