<?php
/**
 * Class that coordinates the plugin's classes and hooks.
 *
 * @package WPezGutenBetterImage\App
 */

namespace WPezGutenBetterImage\App;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

use WPezGutenBetterImage\App\{
	Lib\HooksRegister\ClassHooksRegister as HooksRegister,
	Config\InterfaceConfig,
	Src\Filters\ClassBetterImageBlockCustomize as Customize,
	Src\ClassBetterImageBlock as BetterImageBlock,
};

/**
 * Coordinates the plugin's classes and hooks.
 */
class ClassPlugin {

	/**
	 * Instance of the ClassHooksRegister class.
	 *
	 * @var object
	 */
	protected $new_hooks_reg;

	/**
	 * Array of the plugin's actions.
	 *
	 * @var array
	 */
	protected $arr_actions;

	/**
	 * Array of the plugin's filters.
	 *
	 * @var array
	 */
	protected $arr_filters;

	/**
	 * Instance of the CoreImage class.
	 *
	 * @var object
	 */
	protected $new_better_image_block;

	/**
	 * Let's construct.
	 *
	 * @param Config $new_config Instance of the ClassConfig class.
	 */
	public function __construct( InterfaceConfig $new_config ) {

			$this->setPropertyDefaults( $new_config );

			$this->actions( false );

			$this->filters( true );

			// this must be last.
			$this->hooksRegister();
	}

	protected function setPropertyDefaults( $new_config ) {

		$this->new_hooks_reg = new HooksRegister();
		$this->arr_actions   = array();
		$this->arr_filters   = array();

		$new_better_image_block_customize = new Customize( $new_config->getPluginSlug(), $new_config->getCssUnitAcceptedValues() );
		$this->new_better_image_block = new BetterImageBlock( $new_config, $new_better_image_block_customize );
	}


	/**
	 * After gathering (below) the arr_actions and arr_filter, it's time to
	 * make some RegisterHook magic
	 */
	protected function hooksRegister() {

		$this->new_hooks_reg->loadActions( $this->arr_actions );

		$this->new_hooks_reg->loadFilters( $this->arr_filters );

		$this->new_hooks_reg->doRegister();
	}

	/**
	 * Setup the plugin's actions. There are currently no actions, only filters.
	 *
	 * @param boolean $bool A flag for toggling the method.
	 *
	 * @return void
	 */
	public function actions( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		/*
		$new_actions = new ClassActions();

		$this->arr_actions[] = [
			'active' => true, // active does not have to be set. it - in ClassRegisterHooks - is default: true
			'hook' => 'TODO',
			'component' => $new_actions,
			'callback' => 'TODO',
			'priority' => 5
		];
		*/
	}

	/**
	 * Setup the plugin's filters.
	 *
	 * @param boolean $bool A flag for toggling the method.
	 *
	 * @return void
	 */
	public function filters( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		$this->arr_filters[] = array(
			'active'        => true,
			'hook'          => 'render_block',
			'component'     => $this->new_better_image_block,
			'callback'      => 'evaluateImg',
			'priority'      => 55,
			'accepted_args' => 2,
		);

		// Ref ('Expand full source code' to see filter): https://developer.wordpress.org/reference/functions/wp_calculate_image_srcset/ .
		$this->arr_filters[] = array(
			'active'        => true,
			'hook'          => 'wp_calculate_image_srcset',
			'component'     => $this->new_better_image_block,
			'callback'      => 'wpCalculateImageSrcset',
			'priority'      => 30,
			'accepted_args' => 5,
		);

		// Ref ('Expand full source code' to see filter): https://developer.wordpress.org/reference/functions/wp_calculate_image_sizes/ .
		//
		// as of v 0.0.9
		// active = false - apparently this filter only fires once for all images, as opposed to once per image. 
		// as a result, we don't need this any more. but the code stays until this is 100% confirmed.
		$this->arr_filters[] = array(
			'active'        => false,
			'hook'          => 'wp_calculate_image_sizes',
			'component'     => $this->new_better_image_block,
			'callback'      => 'wpCalculateImageSizes',
			'priority'      => 20,
			'accepted_args' => 5,
		);

	}
}
