<?php
/**
 * The interface that defines the methods for customizing various plugin settings / values.
 *
 * @package WPezGutenBetterImage\App\Src\Filters
 */

namespace WPezGutenBetterImage\App\Src\Filters;

/**
 * Defines the methods for customizing various plugin settings / values. This "bottleneck" enables the plugin to decouple the validation and such from the crux of the code.
 */
interface InterfaceBetterImageBlockCustomize {

	/**
	 * Construct the class.
	 *
	 * @param string $str_plugin_slug The plugin's slug from the config.
	 * @param array  $arr_css_unit The plugin's css unit from the config.
	 */
	public function __construct( string $str_plugin_slug, array $arr_css_unit );


	/**
	 * Which GB align value will we process.
	 *
	 * @param array  $arr_align_process An array of align value.
	 * @param string $str_block_content The block content.
	 * @param array  $arr_attrs The block attrs.
	 *
	 * @return array
	 */
	public function alignProcess( array $arr_align_process, string $str_block_content, array $arr_attrs ) : array;


	/**
	 * Customize the $content_width.
	 *
	 * @param float  $num Current content width.
	 * @param string $context The __FUNCTION__ calling this method.
	 *
	 * @return float Content width to use.
	 */
	public function contentWidth( float $num, string $context) : float;

	/**
	 * Customize the css_unit (e.g., px, em, rem) used.
	 *
	 * @param string $str_unit Current css unit.
	 * @param string $str_align GB image alignment (e.g., left, center, wide, etc.).
	 * @param string $str_block_content GB block content (markup).
	 * @param array  $arr_attrs The attrs for the block.
	 *
	 * @return string The css_unit to use.
	 */
	public function cssUnit( string $str_unit, string $str_align, string $str_block_content, array $arr_attrs ) : string;


	/**
	 * Customize the sizes array for align: full.
	 *
	 * @param array  $arr_sizes Array of current sizes.
	 * @param string $str_block_content GB block content (markup).
	 * @param array  $arr_attrs The attrs for the block.
	 *
	 * @return array Array of sizes to use.
	 */
	public function sizesAlignFull( array $arr_sizes, string $str_block_content, array $arr_attrs ) : array;


	/**
	 * The align wide image width is the $content_width multiplied by this value to "automatically" calculate the align wide width.
	 *
	 * @param float $num_align_wide_ratio Current value of align wide ratio.
	 * 
	 * @return float Align wide ratio to be used.
	 */
	public function sizesAlignWideRatio( float $num_align_wide_ratio ) : float;


	/**
	 * The align wide image width is this fixed value (that will override the one calculated by the align wide ration * content width).
	 *
	 * @param float $num_align_wide_fixed The current align width value.
	 *
	 * @return string The align width value to use.
	 */
	public function sizesAlignWideFixed( float $num_align_wide_fixed ) : float;


	/**
	 * Customize the sizes array for align: wide.
	 *
	 * @param array  $arr_sizes Array of current sizes.
	 * @param string $str_block_content GB block content (markup).
	 * @param array  $arr_attrs The attrs for the block.
	 * @return array Array of sizes to use.
	 */
	public function sizesAlignWide( array $arr_sizes, string $str_block_content, array $arr_attrs ) : array;


	/**
	 * Customize the sizes array for align: center.
	 *
	 * @param array  $arr_sizes Array of current sizes.
	 * @param string $str_block_content GB block content (markup).
	 * @param array  $arr_attrs The attrs for the block.
	 * @param float  $num_width The current value of content width.
	 * 
	 * @return array
	 */
	public function sizesAlignCenter( array $arr_sizes, string $str_block_content, array $arr_attrs, float $num_width ) : array;

	/**
	 * Customize the sizes array for align: left.
	 *
	 * @param array  $arr_sizes Array of current sizes.
	 * @param string $str_block_content GB block content (markup).
	 * @param array  $arr_attrs The attrs for the block.
	 * @param float  $num_width The current value of content width.
	 * 
	 * @return array
	 */
	public function sizesAlignLeft( array $arr_sizes, string $str_block_content, array $arr_attrs, float $num_width ) : array;

	/**
	 * Customize the sizes array for align: right.
	 *
	 * @param array  $arr_sizes Array of current sizes.
	 * @param string $str_block_content GB block content (markup).
	 * @param array  $arr_attrs The attrs for the block.
	 * @param float  $num_width The current value of content width.
	 * 
	 * @return array
	 */
	public function sizesAlignRight( array $arr_sizes, string $str_block_content, array $arr_attrs, float $num_width ) : array;

	/**
	 * Customize the block content markup.
	 *
	 * @param string $str_block_content GB block content (markup).
	 * @param string $str_align GB image alignment (e.g., left, center, wide, etc.).
	 * @param array  $arr_sizes Current array of sizes for this image block.
	 * @param array  $arr_sizes_custom The customized sizes (can be empty).
	 * @param array  $arr_attrs The GB attrs for the block.
	 * @param float  $num_width The current value of content width.
	 *
	 * @return string The block content markup.
	 */
	public function modifyBlockContent( string $str_block_content, string $str_align, array $arr_sizes, array $arr_sizes_custom, array $arr_attrs, float $num_width ) :string;


	/**
	 * For align: wide, the max width can be calculated (content width * srcset_max_width_ratio).
	 *
	 * @param float  $num_max_width_ratio The float that the content width should be multiplied by.
	 * @param string $str_align GB image alignment (e.g., left, center, wide, etc.).
	 *
	 * @return float
	 */
	public function srcsetMaxWidthRatio( float $num_max_width_ratio, string $str_align ) : float;


	/**
	 * Customize the min width (in px) of images included in the srcset.
	 *
	 * @param integer $int_min_width The max width (in px). Images less than this width will be removed from the srcset.
	 * @param string  $str_align GB image alignment (e.g., left, center, wide, etc.).
	 * @param array   $arr_wp_args An array that includes the follow WP args from from hook: sources, size_array, image_src, image_meta, attachment_id.
	 *
	 * @return integer The max width used to "filter" the set of images in the srcset. TODO return 0 to bypass this "filtering."
	 */
	public function srcsetMinWidth( int $int_min_width, string $str_align, array $arr_wp_args ) : int;


	/**
	 * Customize the max width (in px) of images included in the srcset.
	 *
	 * @param integer $int_max_width The max width (in px). Images wider than this width will be removed from the srcset.
	 * @param string  $str_align GB image alignment (e.g., left, center, wide, etc.).
	 * @param array   $arr_wp_args An array that includes the follow WP args from from hook: sources, size_array, image_src, image_meta, attachment_id.
	 *
	 * @return integer The max width used to "filter" the set of images in the srcset. TODO return 0 to bypass this "filtering."
	 */
	public function srcsetMaxWidth( int $int_max_width, string $str_align, array $arr_wp_args ) : int;

}
