<?php
/**
 * The core functionality of the plugin.
 * 
 * @package WPezGutenBetterImage\App\Src
 */

namespace WPezGutenBetterImage\App\Src;

use WPezGutenBetterImage\App\Config\InterfaceConfig as Config;
use WPezGutenBetterImage\App\Src\Filters\InterfaceBetterImageBlockCustomize as Customize;



// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * The core functionality - methods for the WP filters in ClassPlugin - of the plugin.
 */
class ClassBetterImageBlock {


	/**
	 * Array of the registered image sizes.
	 *
	 * @var array
	 */
	private $arr_reg_img_sizes;

	/**
	 * Instance of the config class.
	 *
	 * @var object
	 */
	private $new_config;

	/**
	 * Instance of the customize class.
	 *
	 * @var object
	 */
	private $new_customize;

	/**
	 * The plugin's slug (from the config).
	 *
	 * @var string
	 */
	private $str_plugin_slug;

	/**
	 * Alignment of the image block being processed.
	 *
	 * @var string
	 */
	private $str_align;

	/**
	 * The image block's sizes
	 *
	 * @var string
	 */
	private $str_sizes;

	/**
	 * Used along with content width to calculate width when align wide.
	 *
	 * @var float
	 */
	private $num_wide_ratio;

	/**
	 * The css unit (e.g., 'pc', 'em', etc.).
	 *
	 * @var string
	 */
	private $str_unit;

	/**
	 * Used along with content width to calculate the align wide's max width of images in the srcset.
	 *
	 * @var float
	 */
	private $num_srcset_max_width_ratio;

	/**
	 * Construct the class.
	 *
	 * @param Config             $new_config Instance of the config class.
	 * @param Customize $new_core_image_customize Instance of the customize class.
	 */
	public function __construct( Config $new_config, Customize $new_core_image_customize ) {

		$this->new_config    = $new_config;
		$this->new_customize = $new_core_image_customize;
		$this->setPropertyDefaults();
	}


	/**
	 * Sets the defaults for the properties.
	 *
	 * @return void
	 */
	private function setPropertyDefaults() {

		// The slug is prefixed to "namespace" critical elements of the plugin.
		$this->resetProperty();
		
		$this->arr_reg_img_sizes          = wp_get_registered_image_subsizes();
		$this->num_wide_ratio             = $this->new_config->getWideRatio();
		$this->str_unit                   = $this->new_config->getCssUnit();
		$this->num_srcset_max_width_ratio = $this->new_config->getSrcsetMaxWidthRatio();
	}

	/**
	 * These properties need to be reset.
	 *
	 * @return void
	 */
	private function resetProperty() {
		$this->str_align = false;
		$this->str_sizes = false;
	}


	/**
	 * The core of the plugin. The crux of the magic happens here.
	 *
	 * @param string $str_block_content The block's markup passed by the hook.
	 * @param array  $arr_block The block's key properties passed buy the hook.
	 *
	 * @return string
	 */
	public function evaluateImg( $str_block_content, $arr_block ) {

		// Not a core/image block? Then skip it (i.e., return).
		if ( ! isset( $arr_block['blockName'] ) || 'core/image' !== $arr_block['blockName'] ) {
			return $str_block_content;
		}

		$this->str_align = false;
		$this->str_sizes = false;

		// No attrs? Or has the bypass class string as a class? Then return.
		if ( ! isset( $arr_block['attrs'] ) || $this->checkClassNameForBypass( $arr_block ) ) {
			return $str_block_content;
		}

		// We're good. Keep going...
		$arr_attrs = $arr_block['attrs'];

		// if the attrs width is MIA we'll create it using the image size width
		if ( ! isset( $arr_attrs['width'] ) && isset( $arr_attrs['sizeSlug'] ) && isset( $this->arr_reg_img_sizes[ $arr_attrs['sizeSlug'] ]['width'] ) ) {

			$arr_attrs['width'] = $this->arr_reg_img_sizes[ $arr_attrs['sizeSlug'] ]['width'];
		}

		$str_align = 'center';
		// TODO - validate the align value?
		if ( isset( $arr_attrs['align'] ) && ! empty( $arr_attrs['align'] ) ) {
			$str_align = trim( $arr_attrs['align'] );
		}

		// We can bypass base on alignment.
		if ( ! in_array( $str_align, $this->new_customize->alignProcess( $this->new_config->getAlignProcess(), $str_block_content, $arr_attrs ), true ) ) {
			return $str_block_content;
		}
		$str_unit      = $this->str_unit;
		$str_unit_temp = $this->new_customize->cssUnit( $str_unit, $str_align, $str_block_content, $arr_attrs );

		// Another bypass of sorts. That is, return '' as a signal for "no need to continue".
		if ( empty( $str_unit_temp ) ) {
			return $str_block_content;
		};

		$str_unit               = $str_unit_temp;
		$content_width_wpez_gbi = $this->new_customize->contentWidth( $this->new_config->getContentWidth(), __FUNCTION__ );
		$num_width              = 0;
		$arr_sizes              = array();

		switch ( $str_align ) {

			case 'full':
				$this->str_align = 'full';
				// The default.
				$arr_sizes[] = '100vw';

				// Customize (via filter)?
				$arr_sizes_custom = $this->new_customize->sizesAlignFull( $arr_sizes, $str_block_content, $arr_attrs );
				$this->str_sizes  = $this->makeSizes( $arr_sizes, $arr_sizes_custom );
				break;

			case 'wide':
				$this->str_align = 'wide';

				// Define the align wide width via a "ratio" (of the content_width).
				$num     = $this->new_customize->sizesAlignWideRatio( $this->num_wide_ratio );
				$str_max = (int) ( $content_width_wpez_gbi * $num ) . $str_unit;
				// Or specify a specific width.
				$temp = $this->new_customize->sizesAlignWideFixed( $num );
				if ( $temp !== $num && 0 !== $temp ) {
					$str_max = $temp . $str_unit;
				};

				// default.
				$arr_sizes[] = '(max-width:' . $str_max . ') 100vw';
				$arr_sizes[] = $str_max;

				// Customize (via filter)?
				$arr_sizes_custom = $this->new_customize->sizesAlignWide( $arr_sizes, $str_block_content, $arr_attrs );
				$this->str_sizes  = $this->makeSizes( $arr_sizes, $arr_sizes_custom );
				break;

			case 'center':
			case 'left':
			case 'right':
				$this->str_align = $str_align;

				// GB does get it right w/ image widths < the content width.
				// But if the image width is (accidentally) greater than the content width, GB doesn't update the sizes. We do!!!
				$num_width = $content_width_wpez_gbi;
				if ( isset( $arr_attrs['width'] ) && is_integer( $arr_attrs['width'] ) && $arr_attrs['width'] < $content_width_wpez_gbi ) {
					$num_width = (float) $arr_attrs['width'];
				}

				$str_max         = $num_width . $str_unit;
				$str_max_default = $str_max;
				// TODO - what if we want a small img to be 100vw at something wider than its width?
				// default.
				$arr_sizes[] = '(max-width:' . $str_max . ') 100vw';
				$arr_sizes[] = $str_max_default;

				// Customize (via filter)?
				if ( 'left' === $str_align ) {
					$arr_sizes_custom = $this->new_customize->sizesAlignLeft( $arr_sizes, $str_block_content, $arr_attrs, $num_width );
				} elseif ( 'right' === $str_align ) {

					$arr_sizes_custom = $this->new_customize->sizesAlignRight( $arr_sizes, $str_block_content, $arr_attrs, $num_width );

				} else {

					$arr_sizes_custom = $this->new_customize->sizesAlignCenter( $arr_sizes, $str_block_content, $arr_attrs, $num_width );
				}

				$this->str_sizes = $this->makeSizes( $arr_sizes, $arr_sizes_custom );
				break;

			default:
				$this->str_sizes = false;
		}

		$str_block_content_new = $this->new_customize->modifyBlockContent( $str_block_content, $str_align, $arr_sizes, $arr_sizes_custom, $arr_attrs, $num_width );

		// filtered and different? if so, then return the new / different.
		if ( $str_block_content_new !== $str_block_content ) {

			return $str_block_content_new;
		}

		// do we have a non-empty array of sizes custom?
		if ( is_array( $arr_sizes_custom ) && ! empty( $arr_sizes_custom ) ) {

			// Make those into a string for sizes= .
			$str_sizes_custom = implode( ', ', $arr_sizes_custom );

			if ( ! empty( esc_attr( $str_sizes_custom ) ) ) {
				// tuck the sizes string into img tag.
				return \str_replace( '<img ', '<img sizes="' . esc_attr( $str_sizes_custom ) . '" ', $str_block_content );
			}
		}
		// if all else fails, return the block_content.
		return $str_block_content;
	}

	/**
	 * Does the block's attrs have a className that contains the override class (string), as defined in the config?
	 *
	 * @param array $arr_block The array block from the render_block hook (action).
	 * 
	 * @return bool
	 */
	protected function checkClassNameForBypass( $arr_block ) : bool {

		if ( isset( $arr_block['attrs']['className'] ) ) {
			// Spaces are added to avoid a "subset" string matching.
			if ( false !== strpos( ' ' . $arr_block['attrs']['className'] . ' ', ' ' . $this->new_config->getClassNameForBypass() . ' ' ) ) {
				return true;
			} 
		}
		return false;
	}


	/**
	 * Creates the img tag's sizes string.
	 *
	 * @param array $arr_sizes TODO.
	 * @param array $arr_sizes_custom TODO.
	 *
	 * @return string
	 */
	protected function makeSizes( array $arr_sizes, array $arr_sizes_custom ) : string {

		// Use the custom sizes, if we have them.
		if ( ! empty( $arr_sizes_custom ) ) {
			$arr_sizes = $arr_sizes_custom;
		}
		// make the sizes string
		return implode( ', ', $arr_sizes );
	}


	/**
	 * Remove images from the srcset.
	 *
	 *  Ref ('Expand full source code' to see filter): https://developer.wordpress.org/reference/functions/wp_calculate_image_srcset/
	 *
	 * @param array  $sources {
	 *     One or more arrays of source data to include in the 'srcset'.
	 *
	 *     @type array $width {
	 *         @type string $url        The URL of an image source.
	 *         @type string $descriptor The descriptor type used in the image candidate string,
	 *                                  either 'w' or 'x'.
	 *         @type int    $value      The source width if paired with a 'w' descriptor, or a
	 *                                  pixel density value if paired with an 'x' descriptor.
	 *     }
	 * }
	 * @param array  $size_array     {
	 *     An array of requested width and height values.
	 *
	 *     @type int $0 The width in pixels.
	 *     @type int $1 The height in pixels.
	 * }
	 * @param string $image_src     The 'src' of the image.
	 * @param array  $image_meta    The image meta data as returned by 'wp_get_attachment_metadata()'.
	 * @param int    $attachment_id Image attachment ID or 0.
	 *
	 * @return array
	 */
	public function wpCalculateImageSrcset( array $sources, array $size_array, string $image_src, array $image_meta, int $attachment_id ) {

		if ( is_string( $this->str_sizes ) ) {

			$content_width_wpez_gbi = $this->new_customize->contentWidth( $this->new_config->getContentWidth(), __FUNCTION__ );

			$num_srcset_max_width_ratio = $this->new_customize->srcsetMaxWidthRatio( $this->num_srcset_max_width_ratio, $this->str_align );

			switch ( $this->str_align ) {
				case 'wide':
					$int_min_width = $this->new_config->getSrcsetMinWidthWide();
					$int_max_width = $this->new_config->getSrcsetMaxWidthWide();
					break;
				case 'center':
				case 'left':
				case 'right':
					$int_min_width = 100;
					$int_max_width = (int) ( $content_width_wpez_gbi * $num_srcset_max_width_ratio );
					break;
				case 'full':
				default:
					// This isn't necessary (it's set above) per se, but it makes this switch more readable.
					$int_min_width = $this->new_config->getSrcsetMinWidthFull();
					$int_max_width = $this->new_config->getSrcsetMaxWidthFull();
					break;
			}
			$arr_wp_args = array(
				'sources'       => $sources,
				'size_array'    => $size_array,
				'image_src'     => $image_src,
				'image_meta'    => $image_meta,
				'attachment_id' => $attachment_id,
			);

			// Customize (filter) the min width.
			$int_min_width = $this->new_customize->srcsetMinWidth( $int_min_width, $this->str_align, $arr_wp_args );

			// Customize (filter) the max width.
			$int_max_width = $this->new_customize->srcsetMaxWidth( $int_max_width, $this->str_align, $arr_wp_args );

			// TODO - what if min > max?? Should we check? 

			// Note: There is a WP Core filter 'max_srcset_image_width' but it has no context, so we'll just do it ourselves.
			foreach ( $sources as $key => $arr ) {
				if ( $key < $int_min_width || $key > $int_max_width ) {
					unset( $sources[ $key ] );
				}
			}
		}
		return $sources;
	}

	/**
	 * If there's a new sizes string from imageEval(), use it?
	 *
	 * Ref ('Expand full source code' to see filter): https://developer.wordpress.org/reference/functions/wp_calculate_image_sizes/
	 *
	 * @param string $sizes         A source size value for use in a 'sizes' attribute.
	 * @param array  $size          Requested image size. Can be any registered image size name, or an array of width and height values in pixels (in that order).
	 * @param string $image_src     The URL to the image file or null.
	 * @param array  $image_meta    The image meta data as returned by wp_get_attachment_metadata() or null.
	 * @param int    $attachment_id Image attachment ID of the original image or 0.
	 *
	 * @return string
	 */
	public function wpCalculateImageSizes( string $sizes, $size, $image_src, $image_meta, $attachment_id ) {

		if ( is_string( $this->str_sizes ) ) {

			$content_width_wpez_gbi = $this->new_customize->contentWidth( $this->new_config->getContentWidth(), __FUNCTION__ );

			// There are conditions where WP BG's sizes is better than WPezGBI's (but unfortunately we don't have the $args to know that until this filter).
			$chk_width = true;
			if ( isset( $size[0] ) && $size[0] < $content_width_wpez_gbi && 'wide' !== $this->str_align && 'full' !== $this->str_align ) {
				$chk_width = false;
			}

			// If we have a string of sizes from imageEval() && WPezGBI is better than vanilla WP / GB then use WPezGBI's sizes.
			if ( $chk_width ) {

				$sizes = $this->str_sizes;
				// Reset key properties.
				$this->resetProperty();
			}
		}

		return $sizes;
	}
}
