<?php
/**
 * The class that defines the settings / default values for the plugin.
 *
 * @package WPezGutenBetterImage\App\Config
 */

namespace WPezGutenBetterImage\App\Config;

/**
 * {@inheritDoc}
 */
class ClassConfig implements InterfaceConfig {

	/**
	 * The plugin's dir.
	 *
	 * @var string
	 */
	private $plugin_dir;

	/**
	 * The plugin's url.
	 *
	 * @var string
	 */
	private $plugin_url;

	/**
	 * The plugin's slug, implode()'ed with a separator.
	 *
	 * @var array
	 */
	private $plugin_slug;


	/**
	 * Array that stores the slugs, indexed by the separators.
	 *
	 * @var array
	 */
	private $arr_plugin_slugs;


	/**
	 * Array of GB align values we're willing to process.
	 *
	 * @var array
	 */
	private $arr_align_process;

	/**
	 * The content_width value used by the plugin if WP's $content_width isn't set.
	 *
	 * @var int
	 */
	private $content_width_wpez_gbi;

	/**
	 * CSS class used to in the block's Advanced > Additional CSS Class(es) as a flag to say "Don't process this one."
	 *
	 * @var string
	 */
	private $class_name_for_bypass;

	/**
	 * Array of values used to validate the css_unit value.
	 *
	 * @var array
	 */
	private $css_unit_accepted_values;

	/**
	 * For example, 'px', 'em', etc.
	 *
	 * @var string
	 */
	private $css_unit;

	/**
	 * The plugin will multiply the content width by this value to define the width for align wide.
	 *
	 * @var float
	 */
	private $wide_ratio;

	/**
	 * When align is full, what is the MIN width of images included in the srcset.
	 *
	 * @var int
	 */
	private $srcset_min_width_full;

	/**
	 * When align is full, what is the MAX width of images included in the srcset.
	 *
	 * @var int
	 */
	private $srcset_max_width_full;

	/**
	 * When align is wide, what is the MIN width of images included in the srcset.
	 *
	 * @var int
	 */
	private $srcset_min_width_wide;

	/**
	 * When align is wide, what is the MAX width of images included in the srcset.
	 *
	 * @var int
	 */
	private $srcset_max_width_wide;

	/**
	 * When align is wide, what is the MAX width of images included in the srcset a calculated by content width multiplied by this value.
	 *
	 * @var int
	 */
	private $srcset_max_width_ratio;


	/**
	 * Construct the config.
	 *
	 * @param string $plugin_dir The plugin's dir.
	 * @param string $plugin_url The plugin's url.
	 */
	public function __construct( string $plugin_dir, string $plugin_url ) {

		$this->plugin_dir = $plugin_dir;

		$this->plugin_url = $plugin_url;

		$this->setPropertyDefaults();

	}

	/**
	 * Set the various property defaults.
	 *
	 * @return void
	 */
	private function setPropertyDefaults() {

		$this->plugin_slug = array( 'wpezplugins', 'guten', 'better', 'image' );

		$this->arr_plugin_slugs = array();

		$this->arr_align_process = array( 'left', 'right', 'center', 'wide', 'full' );

		$this->content_width_wpez_gbi = 1100;

		$this->class_name_for_bypass = 'wpez-gbi-false';

		// https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Values_and_units .
		$this->css_unit_accepted_values = array( 'px', 'cm', 'mm', 'Q', 'in', 'pc', 'pt', 'em', 'ex', 'ch', 'rem', 'lh', 'vw', 'vh', 'vmin', 'vmax' );

		$this->css_unit = 'px';

		$this->wide_ratio = 1.33;

		$this->srcset_min_width_full = 400;

		$this->srcset_max_width_full = 4096;

		$this->srcset_min_width_wide = $this->srcset_min_width_full;

		$this->srcset_max_width_wide = (int) ( $this->srcset_max_width_full * .75 );

		$this->srcset_max_width_ratio = 1.75;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getPluginDir() : string {

		return trim( $this->plugin_dir );
	}

	/**
	 * {@inheritDoc}
	 */
	public function getPluginSlug( string $separator = '_' ) : string {

		// Let's not reinvent the wheel.
		$separator = trim( $separator );
		if ( ! empty( $this->arr_plugin_slug[ $separator ] ) ) {
			return $this->arr_plugin_slug[ $separator ];
		}

		// No wheel invented yet? Invent it then.
		if ( '_' === $separator || '-' === $separator ) {
			$this->arr_plugin_slug[ $separator ] = trim( implode( $separator, $this->plugin_slug ) );
			return $this->arr_plugin_slug[ $separator ];
		}

		return __( 'Error: getPluginSlug() $separator must be _ or -.', 'wpez-gbi' );
	}


	/**
	 * {@inheritDoc}
	 */
	public function getAlignProcess() : array {

		return $this->arr_align_process;
	}


	/**
	 * {@inheritDoc}
	 */
	public function getContentWidth() : int {

		global $content_width;

		if ( null !== $content_width ) {
			return $content_width;
		}
		return $this->content_width_wpez_gbi;
	}


	/**
	 * {@inheritDoc}
	 */
	public function getClassNameForBypass() : string {

		return trim( $this->class_name_for_bypass );
	}


	/**
	 * {@inheritDoc}
	 */
	public function getCssUnit() : string {

		return trim( $this->css_unit );
	}


	/**
	 * {@inheritDoc}
	 */
	public function getCssUnitAcceptedValues() : array {

		return $this->css_unit_accepted_values;
	}


	/**
	 * {@inheritDoc}
	 */
	public function getWideRatio() : float {

		return $this->wide_ratio;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getSrcsetMinWidthFull() : int {

		return $this->srcset_min_width_full;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getSrcsetMaxWidthFull() : int {

		return $this->srcset_max_width_full;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getSrcsetMinWidthWide() : int {

		return $this->srcset_min_width_wide;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getSrcsetMaxWidthWide() : int {

		return $this->srcset_max_width_wide;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getSrcsetMaxWidthRatio() : float {

		return $this->srcset_max_width_ratio;
	}

}
