<?php
/**
 * The interface that defines the settings / default values for the plugin.
 *
 * @package WPezGutenBetterImage\App\Config
 */

namespace WPezGutenBetterImage\App\Config;

/**
 * Defines the settings / default values methods for the plugin.
 */
interface InterfaceConfig {

	/**
	 * Construct the class.
	 *
	 * @param string $plugin_dir The plugin's dir as passed in from the bootstrap.
	 * @param string $plugin_url The plugin's url as passed in from the bootstrap.
	 */
	public function __construct( string $plugin_dir, string $plugin_url );

	/**
	 * Return the plugin's dir as passed in via the __construct();
	 *
	 * @return string
	 */
	public function getPluginDir() : string;


	/**
	 *  Return the plugin's slug, with either _ or - as a separator.
	 *
	 * @param string $separator The slug can have a separator of '_' or -.
	 *
	 * @return string
	 */
	public function getPluginSlug( string $separator = '_' ) : string;


	/**
	 * Which align value are we going to re-process.
	 *
	 * @return array
	 */
	public function getAlignProcess() : array;


	/**
	 * Return the plugin's default value for content width.
	 *
	 * @return integer
	 */
	public function getContentWidth() : int;


	/**
	 * If this string is entered in an image block's Advanced > Additional CSS Class(es) input it tells the plugin "skip this one."
	 *
	 * @return string
	 */
	public function getClassNameForBypass() : string;


	/**
	 * Return the plugin's default value for the css unit (e.g., 'px', 'em', etc. ).
	 *
	 * @return string
	 */
	public function getCssUnit() : string;


	/**
	 * Array of values used to validate the css unit value.
	 *
	 * @return array
	 */
	public function getCssUnitAcceptedValues() : array;

	/**
	 * Return the plugin's default value for wide ratio. This value is used to multiply the content width to calculate the align wide's width.
	 *
	 * @return float
	 */
	public function getWideRatio() : float;

	/**
	 * Return the plugin's default value for the MIN width (in px) of an image included in the srcset when align is full.
	 *
	 * @return integer
	 */
	public function getSrcsetMinWidthFull() : int;

	/**
	 * Return the plugin's default value for the MAX width (in px) of an image included in the srcset when align is full.
	 *
	 * @return integer
	 */
	public function getSrcsetMaxWidthFull() : int;

	/**
	 * Return the plugin's default value for the MIN width (in px) of an image included in the srcset when align is wide.
	 *
	 * @return integer
	 */
	public function getSrcsetMinWidthWide() : int;

	/**
	 * Return the plugin's default value for the MAX width (in px) of an image included in the srcset when align is wide.
	 *
	 * @return integer
	 */
	public function getSrcsetMaxWidthWide() : int;

	/**
	 * Return the plugin's default value for what float the content width is multiplied by to define the max width of an image included in the srcset when align is full.
	 *
	 * @return float
	 */
	public function getSrcsetMaxWidthRatio() : float;

}
